window.by_id = null

const row2entry = (
  [dept_name, dept_id, fd_id, status, inactive_date, county, fein, dept_phone,
   dept_fax, fire_ind, rescue_ind, ems_ind, first_responder_ind,
   county_rescue_ind, part_mun_gov_ind, mun_incorp_date, charter_date,
   amended_date, dept_cert_date, inspector, update_date, updated_by,
   merged_with, osfm_inspected_ind, comm_center, frf_participant_id, frf_gov,
   frf_gov_mun_district, frf_delegated_ind, grant_participation_ind, population,
   fire_chief_name, fire_chief_email, fire_chief_work_phone,
   fire_chief_mobile_phone, rescue_chief_name, rescue_chief_email,
   rescue_chief_work_phone, rescue_chief_mobile_phone, ems_chief_name,
   ems_chief_email, ems_chief_work_phone, ems_chief_mobile_phone, dept_address,
   eq_rescue_level, eq_als_ind, eq_confined_space_ind, eq_high_angle_ind,
   eq_alpine_mtn_ind, eq_struct_collapse_ind, eq_trench_ind, eq_water_dive_ind,
   eq_water_ice_ind, eq_water_ocean_surf_ind, sq_water_surface_inc,
   eq_water_swift_ind, eq_wild_lang_search_ind, fire_fighters, volunteer, paid]
) => (
  [
    fd_id,
    {
      dept_id: dept_id,
      status: status,
      inactive_date: inactive_date,
      county: county,
      fein: fein,
      dept_phone: dept_phone,
      dept_fax: dept_fax,
      fire_ind: fire_ind,
      rescue_ind: rescue_ind,
      ems_ind: ems_ind,
      first_responder_ind: first_responder_ind,
      county_rescue_ind: county_rescue_ind,
      part_mun_gov_ind: part_mun_gov_ind,
      mun_incorp_date: mun_incorp_date,
      charter_date: charter_date,
      amended_date: amended_date,
      dept_cert_date: dept_cert_date,
      inspector: inspector,
      update_date: update_date,
      updated_by: updated_by,
      merged_with: merged_with,
      osfm_inspected_ind: osfm_inspected_ind,
      comm_center: comm_center,
      frf_participant_id: frf_participant_id,
      frf_gov: frf_gov,
      frf_gov_mun_district: frf_gov_mun_district,
      frf_delegated_ind: frf_delegated_ind,
      grant_participation_ind: grant_participation_ind,
      population: population,
      fire_chief_name: fire_chief_name,
      fire_chief_email: fire_chief_email,
      fire_chief_work_phone: fire_chief_work_phone,
      fire_chief_mobile_phone: fire_chief_mobile_phone,
      rescue_chief_name: rescue_chief_name,
      rescue_chief_email: rescue_chief_email,
      rescue_chief_work_phone: rescue_chief_work_phone,
      rescue_chief_mobile_phone: rescue_chief_mobile_phone,
      ems_chief_name: ems_chief_name,
      ems_chief_email: ems_chief_email,
      ems_chief_work_phone: ems_chief_work_phone,
      ems_chief_mobile_phone: ems_chief_mobile_phone,
      dept_address: dept_address,
      eq_rescue_level: eq_rescue_level,
      eq_als_ind: eq_als_ind,
      eq_confined_space_ind: eq_confined_space_ind,
      eq_high_angle_ind: eq_high_angle_ind,
      eq_alpine_mtn_ind: eq_alpine_mtn_ind,
      eq_struct_collapse_ind: eq_struct_collapse_ind,
      eq_trench_ind: eq_trench_ind,
      eq_water_dive_ind: eq_water_dive_ind,
      eq_water_ice_ind: eq_water_ice_ind,
      eq_water_ocean_surf_ind: eq_water_ocean_surf_ind,
      sq_water_surface_inc: sq_water_surface_inc,
      eq_water_swift_ind: eq_water_swift_ind,
      eq_wild_lang_search_ind: eq_wild_lang_search_ind,
      fire_fighters: fire_fighters,
      volunteer: volunteer,
      paid: paid
    }
  ]
)

const getSearchString = () => (
  document.querySelector('#form [type=text]').value
)

const setSearchString = (str) => (
  document.querySelector('#form [type=text]').value = str
)

const alwaysShownFields = [
  'fd_id',
  'county',
  'dept_name',
  'dept_phone',
  'dept_fax',
  'inspector',
  'fire_chief_work_phone',
  'fire_chief_mobile_phone',
  'fire_chief_name',
  'fire_chief_email'
]

const defaultHiddenFields = [
  'dept_id',
  'status',
  'inactive_date',
  'fein',
  'fire_ind',
  'rescue_ind',
  'ems_ind',
  'first_responder_ind',
  'county_rescue_ind',
  'part_mun_gov_ind',
  'mun_incorp_date',
  'charter_date',
  'amended_date',
  'dept_cert_date',
  'update_date',
  'updated_by',
  'merged_with',
  'osfm_inspected_ind',
  'comm_center',
  'frf_participant_id',
  'frf_gov',
  'frf_gov_mun_district',
  'frf_delegated_ind',
  'grant_participation_ind',
  'population',
  'rescue_chief_name',
  'rescue_chief_email',
  'rescue_chief_work_phone',
  'rescue_chief_mobile_phone',
  'ems_chief_name',
  'ems_chief_email',
  'ems_chief_work_phone',
  'ems_chief_mobile_phone',
  'dept_address',
  'eq_rescue_level',
  'eq_als_ind',
  'eq_confined_space_ind',
  'eq_high_angle_ind',
  'eq_alpine_mtn_ind',
  'eq_struct_collapse_ind',
  'eq_trench_ind',
  'eq_water_dive_ind',
  'eq_water_ice_ind',
  'eq_water_ocean_surf_ind',
  'sq_water_surface_inc',
  'eq_water_swift_ind',
  'eq_wild_lang_search_ind',
  'fire_fighters',
  'volunteer',
  'paid'
]

const resultsTableHTML = (obj) => {
  const klass = (field) => (
    alwaysShownFields.includes(field) ? 'default-shown' : 'default-hidden'
  )
  const trs = alwaysShownFields.concat(defaultHiddenFields).map((field) => (
    `<tr class="${klass(field)}">
       <td>${field.replace(/_/g, ' ')}</td>
       <td>${obj[field] === null ? '-' : obj[field]}</td>
     </tr>`
  ))
  const thead = `<thead><tr><th>field</th><th>value</th></tr></thead>`
  const tbody = `<tbody>${trs.join('')}</tbody>`
  return `<table>${thead}${tbody}</table>`
}

const updateResults = () => {
  const searchString = getSearchString()
  if (!searchString) return
  const invalid = !searchString.match(/^\d{5}$/)
  const notFound = !by_id.has(searchString)
  document.getElementById('results').innerHTML =
    invalid ? `Error: I expected 5 numbers, e.g. <code>01234</code>, but you searched for '<code>${searchString}</code>'.`
    : notFound ? `I don't have any records of a fire department with ID '<code>${searchString}</code>'.`
    : resultsTableHTML(by_id.get(searchString))
  return !(invalid || notFound)
}

const updateExpanded = (expanded) => {
  document.querySelectorAll('.default-hidden').forEach((el) => {
    el.classList.toggle('hidden', !expanded)
  })
  document.getElementById('show-more').classList.toggle('hidden',  expanded)
  document.getElementById('show-less').classList.toggle('hidden', !expanded)
}

window.addEventListener('load', (_) => {
  window.by_id = new Map(data.map(row2entry))
  document.getElementById('form').addEventListener('submit', (evt) => {
    evt.preventDefault()
    window.history.pushState({}, '', `?s=${getSearchString()}`)
    updateResults() && updateExpanded(false)
  })
  document.getElementById('show-more').addEventListener('click', (evt) => {
    evt.preventDefault()
    updateExpanded(true)
  })
  document.getElementById('show-less').addEventListener('click', (evt) => {
    evt.preventDefault()
    updateExpanded(false)
  })
  const queryString = document.location.search
  setSearchString(queryString.substr(3))
  updateResults() && updateExpanded(false)
  const inputEl = document.querySelector('#form [type=text]')
  const n = inputEl.value.length
  inputEl.focus()
  inputEl.setSelectionRange(n, n)
})
