# NC OSFM Fire Department Lookup Tool

This is a static web app. To run a local development version, just start a file
server in the current directory. (See [this list of
one-lines](https://gist.github.com/willurd/5720255 ) if you're not sure how to
do this.)

The web app is also deployed via Netlify at
[https://modest-darwin-2d69db.netlify.app/](https://modest-darwin-2d69db.netlify.app/).

If you need to refresh the data:

1. [install Babashka](https://github.com/babashka/babashka#installation);
2. get a current data dump from Adam in the same format as the
   `NCFD_Export_20220201.csv` file in the repo, which we'll call `CSVFILE`
   below;
3. cd to the project root directory (i.e. the one containing this `README.md`
   file);
4. delete the `data.csv` file (don't worry, git has it saved); and
5. run `bb run import CSVFILE`.

Then, if no error message was displayed, `data.csv` should be updated according
to the newest data. If you commit the new `data.csv` file and push the commit
to the GitLab repo, Netlify will automatically deploy it to the aforementioned
site within a minute or so.

Questions? Email Jeff Terrell
&lt;[jeff.terrell@acm.org](jeff.terrell@acm.org)&gt;.
