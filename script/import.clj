#!/usr/bin/env bb
(ns import
  (:require clojure.data.json
            [clojure.java.io :as io]))

(defn- read-data [infile]
  (let [[colnames & data] (with-open [reader (if (= "-" infile)
                                               *in*
                                               (io/reader infile))]
                            (-> reader clojure.data.csv/read-csv vec))]
    (if (not= colnames ["﻿DEPT_NAME" "DEPT_ID" "FD_ID" "STATUS" "INACTIVE_DATE"
                        "COUNTY" "FEIN" "DEPT_PHONE" "DEPT_FAX" "FIRE_IND"
                        "RESCUE_IND" "EMS_IND" "FIRST_RESPONDER_IND"
                        "COUNTY_RESCUE_IND" "PART_MUN_GOV_IND" "MUN_INCORP_DATE"
                        "CHARTER_DATE" "AMENDED_DATE" "DEPT_CERT_DATE"
                        "INSPECTOR" "UPDATE_DATE" "UPDATED_BY" "MERGED_WITH"
                        "OSFM_INSPECTED_IND" "COMM_CENTER" "FRF_PARTICIPANT_IND"
                        "FRF_GOV" "FRF_GOV_MUN_DISTRICT" "FRF_DELEGATED_IND"
                        "GRANT_PARTICIPANT_IND" "POPULATION" "FIRE_CHIEF_NAME"
                        "FIRE_CHIEF_EMAIL" "FIRE_CHIEF_WORK_PHONE"
                        "FIRE_CHIEF_MOBILE_PHONE" "RESCUE_CHIEF_NAME"
                        "RESCUE_CHIEF_EMAIL" "RESCUE_CHIEF_WORK_PHONE"
                        "RESCUE_CHIEF_MOBILE_PHONE" "EMS_CHIEF_NAME"
                        "EMS_CHIEF_EMAIL" "EMS_CHIEF_WORK_PHONE"
                        "EMS_CHIEF_MOBILE_PHONE" "DEPT_ADDRESS"
                        "EQ_RESCUE_LEVEL" "EQ_ALS_IND" "EQ_CONFINED_SPACE_IND"
                        "EQ_HIGH_ANGLE_IND" "EQ_ALPINE_MTN_IND"
                        "EQ_STRUCT_COLLAPSE_IND" "EQ_TRENCH_IND"
                        "EQ_WATER_DIVE_IND" "EQ_WATER_ICE_IND"
                        "EQ_WATER_OCEAN_SURF_IND" "EQ_WATER_SURFACE_IND"
                        "EQ_WATER_SWIFT_IND" "EQ_WILD_LAND_SEARCH_IND"
                        "FIRE_FIGHTERS" "VOLUNTEER" "PAID"])
      (throw (new Exception "Expected different column names. Manual checking required."))
      (clojure.data.json/write-str (map (fn [row] (map #(if (= % "NULL") nil %)
                                                       row))
                                        data)))))

(defn- print-help []
  (binding [*out* *err*]
    (println "Synopsis: convert CSV dump to a convenient JSON format.")
    (println "Specify input CSV file as the only argument (or - for stdin).")))

(defn- die [msg]
  (binding [*out* *err*]
    (println "Error:" msg)
    (println "(Use -h for help.)")))

;; "NCFD_Export_20220201.csv"
(defn- main []
  (cond
    (some #{"-h" "--help"} *command-line-args*) (print-help)
    (not= 1 (count *command-line-args*)) (die "specify only the input file as an argument")
    :else (let [infile (first *command-line-args*)
                outfile (io/file "data.js")]
            (when (not= "-" infile)
              (assert (not (.exists outfile)) "Error: data.js exists"))
            (binding [*out* (io/writer outfile)]
              (println "window.data =" (read-data infile)))
            (binding [*out* *err*] (println "saved output as data.js")))))
